<?php
defined( 'ABSPATH' ) || die();

//[doggo]
add_shortcode('doggo', 'doggoHandle');
function doggoHandle($params) {

    $mode = $params[0];

    switch (strtolower($mode)) {
        case "samoyed":
            $html = Doggo::setSamoyed();
            break;
        case "shiba":
            $html = Doggo::setShiba();
            break;
        default:
            $html = Doggo::setSamoyed();
            break;
    }

    return $html;
}
