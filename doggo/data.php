<?php


class Doggo
{
    public static function getRandomDog($breed): string{
        $ch = curl_init();
        $url = "https://dog.ceo/api/breed/{$breed}/images/random";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($ch);
        curl_close($ch);
        $html = json_decode($html);
        return $html->message;
    }

    public static function setSamoyed(){
        echo "<img class=\"dog-img\" src=\"" . self::getRandomDog("samoyed") . "\" alt=\"doggo\">";
    }
    public static function setShiba(){
        echo "<img class=\"dog-img\" src=\"" . self::getRandomDog("shiba") . "\" alt=\"doggo\">";
    }
}
