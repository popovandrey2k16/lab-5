<?php
/*
Plugin Name: Случайное изображение собаки
Description: Получить случайное изображение собаки
Version: 1.0
Author: Алексей и Андрей
Author URI: https://youtu.be/1yDXommZwWQ
*/

defined( 'ABSPATH' ) || die();
define('DOGGO_DIR', dirname( __FILE__ ));

require_once DOGGO_DIR . "/data.php";
require_once DOGGO_DIR . "/shortcode.php";